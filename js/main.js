(function($) {
    var gutters = function () {
            var headerH = $('#header').height(),
                realH = headerH + 8,
                winW = $(window).width();
            $('#gutters').css({
                "border-width": 8 + "px"
            });
            $('#header').css({
                "top": 8 + "px"
            });
            $('#main').css('margin-top', headerH);
            if (winW < 768) {
                $('#mob-btn').css({
                    "bottom": 8 + "px"
                });
            }
    };
    $(window).on('load', function() {
        gutters();
    });
    var menu_mt = function() {
        var headerH = $('#header').height(),
            realH = headerH + 8,
            winW = $(window).width();
        if (winW < 768) {
            $('.offcanvas-collapse').css('top', realH);
        }
    };
    var burgerMenu = function () {
        $('.menu').click(function () {
            $('.offcanvas-collapse').toggleClass('open');
            if ($('.offcanvas-collapse').hasClass('open')) {
                menu_mt();
                $('.burger').addClass('active');
            } else {
                $('.burger').removeClass('active');
            }
            $('body').toggleClass("no-scroll");
            $('#mob-btn').toggleClass('active');
        });
    };
    var windowScroll = function () {
        $(window).scroll(function () {
            var header = $('#header'),
                scroll = $(window).scrollTop();
            if (scroll >= 80) {
                $(header).addClass('fixed');
                menu_mt();
            } else {
                $(header).removeClass('fixed');
                menu_mt();
            }
        });
    };
    var goToTop = function() {
        $('.js-gotop').on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $('html').offset().top
            }, 500, 'easeInOutExpo');
            return false;
            menu_mt();
        });
        $(window).scroll(function() {
            var $win = $(window);
            if ($win.scrollTop() > 200) {
                $('.js-top').addClass('active');
            } else {
                $('.js-top').removeClass('active');
            }
        });
    };
    $(function() {
        gutters();
        burgerMenu();
        windowScroll();
        goToTop();
    });
})(jQuery)