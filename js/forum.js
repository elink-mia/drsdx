$(function() {
    var dec_height = function() {
        var content = $('#page').height(),
            win_h = $(window).height(),
            main_h = $('#main').innerHeight(),
            need_h = win_h - content - 16;
        if (content < win_h) {
            $('#main').css("min-height", main_h + need_h);
        }
        $(window).resize(function() {
            if (content < win_h) {
                $('#main').css("min-height", main_h + need_h);
            }
        });
    };
    $(window).on('load', function() {
        dec_height();
    });
    ft_bort();
});
var ft_bort = function() {
    if  ($('#main').hasClass('no-ad')) {
        $('#footer').addClass('bor-t');
    } else {
        $('#footer').removeClass('bor-t');
    }
};